# Introduction

![nlang](icon.png)

**Neutre** language is an experimental coding
model and syntax.

Unlike many modern language **Neutre** is not made to make you code fast and dirty.
**Neutre** aims for long shoot projects, favors easy ready with few keywords, 
static types, code constraints, forced exceptions checks, and so on.
**Neutre** is mainly for back-end libraries but can still be used as a script.

## Goals
- fast learning
- obvious coding
- constrainted coding
- exception safe
- simple parsing
- VM/Compiler independant
- Public Domain

## VM/Compiler

**Neutre** will come with a reference minimalist virtual machine using Unlicense.science library.
But those are two distinct projects.

## History

I started programming around 2002 in C and ASM for micro-controlers, later on learned
VisualBasic, PHP, JSP, JavaScript, Java while I was student. Java 1.4 solved all my needs
at this time (2003) and still today I use it at work.
But in the years 2009-2012 after Oracle bought Java the ecosystem started to become
less friendly, many projects moved aways or died, Hudson-Jenkins, OpenOffice-LibreOffice, 
OpenSolaris, Glassfish, NetBeans, and so on...
So I started (and still do) a wide sweep around to look for other solutions, C#, Ruby, Python, ParrotVM,
R, Julia, Scala, Kotlin, C++, Fortress, D, ...
Ruby, Python and D are I believe good alternatives but still they lake the robust
aspect of Java.

Many languages claim rich and consice syntaxes are the best but I believe they are
wrong, completely. If my 10+ years of programming have teach me something, it's that
a good language is one which allows you to read and understand someone else's code 
in one pass.On this aspect Ruby,Python have the upper-hand and Scala,C++ the lasts.

**Neutre** is my own reflection on what would be a reliable language capable
of replacing java. I started this work in 2014 working with ParrotVM pseudo-code
to build something, and rewrote several times the syntax until end 2017 when I'm
started to feel it's done *right*. There are still questions remaining like
for concurrency and constraints but it should remain mainly the same.


