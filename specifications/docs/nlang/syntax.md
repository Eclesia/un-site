# Comments


Single line comments must be alone on their line.
They can not be placed after some code on the end of a line.
```
# x value affectation
x <- 15
```

Multi lines comments
```
###
This function adds two integer values
and returns the result
###
add <- Function(Int32 a, Int32 b / Int32 r) {
    r <- a + b
}
```


# Keywords

### Global
- use
- as
- <-
- self

### Control flow
- case
- loop
- next
- stop
- safe
- unsafe
- return
- fail

### Objects
- Function
- Method
- Class
- Prototype


# variables

**Neutre** makes use of type inference.
Eveything is an Object, there are no primitives.

Variables can be declared with a value in which case the type is the same
as the value, and/or the type can explicitly declared.
```
# Declare variable without affectation
x : Int32
myCar : Car

# Declare and assign value
y <- 10
user <- User.create('Jean')

# Declare, explicit type and value
z : Number <- 10
```

# Flow controls

Unlike many language **Neutre** has only 2 keywords to achieve the same result
as 'if-else', 'while', 'for', 'do-while'.

There is no 'switch->fall-through' like syntax.
From a quick survey of developers it appears this usage is very rare and can be
avoided in the worse case with very few duplicated lines.
Adding a special keyword for this behavior, adding complexity both to
a parser and a compiler is not worse it.


```
# single condition, if style
case (x < 10)
    b <- 20
case

# short writing
case! (x < 10) b <- 20

# if-elseif-else writing
case (x < 10)
    b <- 20
case (x < 100)
    b <- 30
case
    b <- 40
case!

# switch writing
text <- ''
case (x < 10)
    text <- text + 'A'
case (x < 100)
    text <- text + 'B'
case (x < 1000)
    text <- text + 'C'

# while style loop
loop (x < 10)
    x <- x + 1
loop!

# do-while style loop
loop
    x <- x + 1
loop! (x < 10)

# for style loop
loop (x <- 1 ; x < 10 ; x <- x++)
    x <- x + 1
loop!

# infinite loop
loop
    x <- x + 1
loop!

# with labels and loop controls
loop:bigname (x <- 1 ; x < 10 ; x <- x++)
    x <- x + 1
    loop:smallname
        case (x > 10)
            next:bigname
            stop:smallname
        case!
    loop!
loop!
```


# Functions

Functions are callable elements which allow multiple inputs and can produce
multiple ouputs.
Unlike many languages functions are not attached to a Class, if it's the case those
are called *Method*.


```
concat <- Function(Chars text, Number num / Chars result) {
    result <- text.append(num.toChars())
}

process <- Function(Int32 a, Int32 b / Int32 na, Int32 nb) {
    na <- a * b
    nb <- a / b
    # return keyword is optional, if can be used to break out of the function
    return
}


words : Chars
x : Int32
y : Int32

words <- concat('A number like ', 15)
x,y <- process(5,10)


```



```
------------------------------------------------
Fonctions
------------------------------------------------


# Global math utils

import science.unlicense:common:+:collection
import science.unlicense:math:+:Vector.create as v

Class Math <- {

    # short description
    -- constant
    Float64 PI <- 3.14f
    UInt128 BIG <- 123456789l
    Vector NORMAL <- v:[45,3]
    Document metadata <- d:'test'
    Document metadata <- t:'test'

    ##
    # long description
    ##
    -- public,final
    -- x : any
    -- y : any
    -- r : any
    Function (Int32 x, Int32 y / Int32 r) add,+ <- {

    }

    Function (Int32 x, Int32 y, Int32 z / Int32 r) sum,∑ <- {

    }

    -- r : positive
    Function (Int32 x / Int32 r) positive <- {
        case (x < 0)
            r <- x
        case
            r <- x
        case!
    }

    Method (Vector x / Vector r) work <- {
        Vector v <- x * 2
        Vector old <- x.scale(2)
        Errors.push('bug')
        --> fail
    }
}


Class Vector <- {

    Float64 x
    Float64 y

    Function (Number x, Number y / Vector r) create <- {
        r <- new
        --> r
    }

    Method (Double s / Vector r) scale,* <- {
        self.x <- self.x * s
        self.y <- self.y * s
        --> r <- self
    }

}


```




```



i += 2
i + 2

i <- i + 2

-- control
i <- i add 1 mul 4
i <- 14.add(1).mul(4)
i <- 14:add(1):mul(4)


Truc c = ...

Voiture v = c.faireTruc(maison);

Voiture v = c fairetruc maison

Voiture v = c ㌒ maison

x : Int32 : notNegative # computed value


class Truc {

	Voiture faireTruc(Maison m) {
		㌐
		u222b
		∫(1,2, )

	}

	Voiture ㌒(Maison m) {


	}

}



x <- 40
x <- y

Int32 x <- a + b + c

Int32 x <- √(a + b) / (4 * 3)
Boolean valid <- true
valid <- !(valid)
valid <- valid !

** public ** Int32 x <- 42


case (query != null) && (query.getFilter() != null)
case (query.notEqual(null)).and((query.getFilter()).notEqual(null))


Function[Int32 / Int32,Int32] callback <- Math:add

Function(Int32 / Int32,Int32) callback <- Int32.+

Function(Int32 / Int32,Int32) callback <- Math.add(1,2).toString()

add,+ : Function(x:Int32, y:Int32 / r:Boolean) <- {

}

add,+ <- Function[x:Int32,y:Int32],[r:Boolean] {

}

Int32 result <- callback(1,2)


Function(/) nothing <- {

}


Function(Int32 x / Int32 a, Int32 b) add <- {
	x <- a + b
	end
}

Function add(Int32 a, Int32 b / Int32 c, Int32 d) <- {

}

Class() Car <- {

	Chars name

	Function(Chars name / Car c) create <- {
		c <- new()
		c.name <- name
	}

}

Class() Transport <- {

	Int32 nbPlace

	Function(Int32 nb / Transport t) create <- {
		t <- new()
		t.nbPlace <- nb
	}

}

Class(Car c, Transport t) Kangoo <- {

	Method( / Kangoo k) <- {
		k <- new( [Car.create("Kangoo"), Transport.create(5)] )
	}

}

Class() Number <- {
	class.ntype
	instance.x

	Method(Number x / class a, class b) +

}

Class(Number) Int32 <- {

	Constructor( / Int32 i) create <- {

	}

	Method(Int32 x / Int32 b) add,+ <- {

	}

    -- scope : public
	Method(Int32 x / Int32 b) sum,∑ <- {}

	Method(Boolean x / Int32 b) < <- {}

	Method(Boolean x / Int32 b) <= <- {}

	Method(Int32 x / Double64 b) + <- {}

	Method(Int32 x / Double64 b) != <- {}
	Method(Int32 x / Double64 b) == <- {}

}


case (a < 10) {

} case (a > 20) {

} case {

}

case  condition

	fall
case ..

case!

loop x=3, x<10, x++

loop!


Class() Int32 <- {

}

Person <- Class[] {

    name : Chars
    firstname : Chars

    Constructor(name:Chars, firstName:Chars / Person r) {
        r <- new
        r.name <- name
        r.firstName <- firstName
    }

}

#generics like
Collection <- Class( / Class T) {

    get <- Method(Int32 index / T )

}


 -- private
User : : private <- Class(Person) {

    age : Int32
    logged <- false

    create <- Function(name:Chars, firstName:Chars, age:Int32 / User r) {
        r <- Person(name,firstName)
        r.age <- age
    }

    login <- Method[][] {
        self.logged <- true
    }

}

-------- creating a class : prototype model : une bonne idée ?

proto <- Prototype()
proto.x : Chars





-- define org.project.scope
-- define org.project.constraint
-- define org.project.given
-- define org.project.public
-- define org.project.final

Class() Boolean <- {

    Int32 value

	# a comment
    # one multiple
    # lines
    -- public
    -- final
	Method(Boolean x / Boolean b) &  <- {}
    -- public,final
	Method(Boolean x / Boolean b) && <- {}
	-- public,final -- Method(Boolean x / Boolean b) |  <- {}
	--public,final -- Method(Boolean x / Boolean b) || <- {}
	* public,final * Method(Boolean x / *given* Boolean b) || <- {}
	- public,final - Method(Boolean x /          ) ! <- {

        -- constraint : x : true

    }

	Method(Int32 x < Int32 b) sum,∑ <- {}

	Method(Boolean x < Int32 b) < <- {}

	Method(Boolean x < Int32 b) <= <- {}

	Method(Int32 x < Double64 b) + <- {}

	Method(Int32 x < Double64 b) √ <- {}

	Method(Int32 x < Double64 b) ∜ <- {}

	Method(Int32 x < Double64 b) ∫ <- {}

	Method(Int32 x < Double64 b) ⎇ <- {}

	Method(Int32 x < Double64 b) ⇲ <- {}

}

Class() Iterator <- {

	Method( / Boolean b) hashNext

	Method( / Object v) next


}

---------------------------------------------------


Prototype() p <-




===========================================
shortcuts
+ add(*,*)
/ div(*,*)





█
█ Adds two values
█
▒ scope:module
▒ shortcuted
main () <- () {

	Int32 x,y,z
    ** notnull(x)
	b <- sub(x,y)
	b <- add(x,y)

}



create (User user) <- () {
	user <- User
}

User {

	create (User user) <- () {
		user <- User:new()
	}


	|| Adds two values
	||
	** scope(public)
	add (UInt16 b) <- (Int32 x, Int32 y) {
		b <- x + (y * x)

		return
	}

}


--------------------------------------

include science.unlicense.library:api-system:Runtime.*

# Function documentation
#
# main entry point
#
-- public, main
Function( / Chars[] args) sayHello <- {
    println('Hello world')
}


========================================================
ANNOTATION
========================================================

 @ : manque de visibilité
 : manque de visibilité
 ! : agressif
 - : ambigue avec math substract
 -- : possible ambigue avec math decrement sauf si toujours en debut de ligne <-- SOLUTION
 --- : trop long
 * : ambigue avec math multiply
 ** : ambigue avec math power
 :: : possible ambigue avec les paths/package
 <+> : trop long a taper
 <...> : ambigue avec operateur sub/over math  <-- SOLUTION

------ examples --------

 	-- public,final
	Method(Boolean x / Boolean b) && <- {}

 	@ public,final
	Method(Boolean x / Boolean b) && <- {}

 	! public,final
	Method(Boolean x / Boolean b) && <- {}

 	<public,final>
	Method(Boolean x / Boolean b) && <- {}

 	:: public,final
	Method(Boolean x / Boolean b) && <- {}

	-- public,final -- Method(Boolean x / Boolean b) |  <- {}

	--public,final-- Method(Boolean x / Boolean b) || <- {}

	* public,final * Method(Boolean x / *given* Boolean b) || <- {}

	** public,final ** Method(Boolean x / *given* Boolean b) || <- {}

	::public,final:: Method(Boolean x / ::given:: Boolean b) || <- {}

	@ public,final @ Method(Boolean x / *given* Boolean b) || <- {}

	Method(Boolean x /          ) work <- {

        -- constraint : x : true
		x <- 5
        :: constraint : x > 20

    }

    # non : les parametres sont associés au nom
	Method compute(
        in Int32 x
        in Int32 y
        out Boolean b
        ) <- {}

    # non : les parametres ne sont pas dans la signature
    Method compute <- {
        in Int32 x
        in Int32 y
        out Boolean b
    }

    # non : serait inconsistant avec la declaration des variables
    compute Method(
        in Int32 x -- between 1 and 10
        in Int32 y --
        out Boolean b
        ) <- {
    }

    #
    # Bien ca
    #
    #
    --
    -- x : between(1,10)
    -- y : notNegative
    -- b : negative
    Method(Int32 x, Int32 y, Float32 b) compute <- {

        -- x : notNull
    }



========================================================
BRANCHEMENT
========================================================

Possible blocks with one-line writing for :

return
fail
safe
unsafe

------ examples --------


loop (x <- 1 | x < 10 | x <- x++)
	work()
loop!

loop (x <- 1 | x < 10 | x <- x++) work()


if (x < 10)
    work()
if!

case (x < 10) work();

Int32 x
case (a < 10)
    x <- 5
case (a < 20)
    x <- 20
case
    x <- 30
case!

# same as a while
loop (x < 10)
    x <- x + 1
loop!

# same as a do-while
loop
    x <- x + 1
loop! (x < 10)

# same as a for
loop (x <- 1 ; x < 10 ; x <- x++)
    x <- x + 1
loop!

loop:bigname (x <- 1 ; x < 10 ; x <- x++)
    x <- x + 1
    loop:smallname
        case (x > 10)
            next:bigname
            stop:smallname
        case!
    loop!
loop!



========================================================
EXCEPTION
========================================================

Error, Exception, Info, Log ?

Facts :
Unchecked exception are a problem for reliability.
Check exception can be painful to always handle.

return
fail
safe
unsafe



------ examples --------

Function(Int32 a < Int32 b | Errors) bug <- {
    -> fail errors.push(Error.ERROR, "big bug")
}


Function(Int32 a < Int32 b | Errors) bug <- {
    case (x < 0) -> fail errors.push(Error.ERROR, "big bug")
}

Function(Int32 a < Int32 b | Errors) bug <- {
    errors.push(Error.ERROR, "big bug", new CustomReport(...))
	-> fail
}

Function(Int32 a < Int32 b | Errors) bug <- {
    errors.push(Error.WARNING, "small imprecision")
    -> return a * 10
}


Function(/) complicate <- {

    # full syntax to catch errors on multiple lines
    safe
        Int32 x <- bug()
        Int32 y <- bug2()
    safe
		errors.get(0)
    safe!

    # syntax ignoring errors
    safe
        Int32 x <- bug()
        Int32 y <- bug2()
    safe!

    # short syntax ignoring error for single instruction
    safe Int32 x <- bug()

    # short syntax pushing error up
    unsafe Int32 x <- bug()

}

========================================================
FUNCTIONAL
========================================================


------ examples --------

Method work <- {

	Function(Int32 a < Int32 b) fff <- {
		...
	}

	Int32 x <- fff(45)

	fff <- self.localMethod

}




========================================================
REFLECTION et IMPORT
========================================================


-- define : org.geotoolkit.map.Context
-- include org.maven.model.MavenProject
<+> include(org.geotoolkit.map.Context)
+ define : org.geotoolkit.map.Context
-- with {moduleGroup}:{moduleId}:{version?}:Math.Double
-- with {moduleGroup}:{moduleId}::Math.*
-- with {moduleGroup}-{moduleId}--Math.*

import {moduleGroup}:{moduleId}:{version?}:Math.Double <- cela fait partie du language, pas juste une contrainte

import science.unlicense:system:3:System as Sys3
import science.unlicense:system:2:System as Sys2


Module module  <- {moduleGroup}:{moduleId}:{version?}:
Package racine <- {moduleGroup}:{moduleId}:{version?}:.
Class double   <- {moduleGroup}:{moduleId}:{version?}:Math.Double
Variable pi    <- {moduleGroup}:{moduleId}:{version?}:Math.PI
Function sum   <- {moduleGroup}:{moduleId}:{version?}:Math.sum
Method add     <- {moduleGroup}:{moduleId}:{version?}:Math.Double.add
Resource icon  <- {moduleGroup}:{moduleId}:{version?}:/data/logo.png

la version est optionelle
Module module  <- {moduleGroup}:{moduleId}:+:
Package racine <- {moduleGroup}:{moduleId}:+:.
Class double   <- {moduleGroup}:{moduleId}:+:Math.Double
Variable pi    <- {moduleGroup}:{moduleId}:+:Math.PI
Function sum   <- {moduleGroup}:{moduleId}:+:Math.sum
Method add     <- {moduleGroup}:{moduleId}:+:Math.Double.add
Resource icon  <- {moduleGroup}:{moduleId}:+:/data/logo.png
//all resources
Resource icon  <- *:*:*:/data/logo.png
//all resources, for services as example
Resource icon  <- *:*:*:/module/meta.tree

------ examples --------

Parent Reflect
Resource <- science.unlicense:system::data/logo.png
Module p <- science.unlicense:system::
Package p <- science.unlicense:system::*
Package p <- science.unlicense:system::Runtime
Variable p <- science.unlicense:system::Math.PI
Class p <- science.unlicense:system::Math.Double
Function p <- science.unlicense:system::Math.sum
Method p <- science.unlicense:system::List.add
Method p <- science.unlicense:common:7.3:collection:List.add

```


```



i += 2
i + 2

i <- i + 2

i <- i add 1 mul 4
i <- 14.add(1).mul(4)
i <- 14:add(1):mul(4)


Truc c = ...

Voiture v = c.faireTruc(maison);

Voiture v = c fairetruc maison

Voiture v = c ㌒ maison



class Truc {

	Voiture faireTruc(Maison m) {
		㌐
		u222b
		∫(1,2, )

	}

	Voiture ㌒(Maison m) {


	}

}



x <- y

Int32 x <- a + b + c

Int32 x <- √(a + b) / (4 * 3)
Boolean valid <- true
valid <- !(valid)
valid <- valid !

** public ** Int32 x <- 42


case (query != null) && (query.getFilter() != null)
case (query.notEqual(null)).and((query.getFilter()).notEqual(null))


Function[Int32 / Int32,Int32] callback <- Math:add

Function(Int32 / Int32,Int32) callback <- Int32.+

Function(Int32 / Int32,Int32) callback <- Math.add(1,2).toString()

Int32 result <- callback(1,2)


Function(/) nothing <- {

}


Function(Int32 x / Int32 a, Int32 b) add <- {
	x <- a + b
	end
}

Function add(Int32 a, Int32 b / Int32 c, Int32 d) <- {

}

Class() Car <- {

	Chars name

	Function(Chars name / Car c) create <- {
		c <- new()
		c.name <- name
	}

}

Class() Transport <- {

	Int32 nbPlace

	Function(Int32 nb / Transport t) create <- {
		t <- new()
		t.nbPlace <- nb
	}

}

Class(Car c, Transport t) Kangoo <- {

	Method( / Kangoo k) <- {
		k <- new( [Car.create("Kangoo"), Transport.create(5)] )
	}

}

Class() Number <- {
	class.ntype
	instance.x

	Method(Number x / class a, class b) +

}

Class(Number) Int32 <- {

	Constructor( / Int32 i) create <- {

	}

	Method(Int32 x / Int32 b) add,+ <- {

	}

    -- scope : public
	Method(Int32 x / Int32 b) sum,∑ <- {}

	Method(Boolean x / Int32 b) < <- {}

	Method(Boolean x / Int32 b) <= <- {}

	Method(Int32 x / Double64 b) + <- {}

	Method(Int32 x / Double64 b) != <- {}
	Method(Int32 x / Double64 b) == <- {}

}


case (a < 10) {

} case (a > 20) {

} case {

}

case  condition

	fall
case ..

case!

loop x=3, x<10, x++

loop!


Class() Int32 <- {

}

-- define org.project.scope
-- define org.project.constraint
-- define org.project.given
-- define org.project.public
-- define org.project.final

Class() Boolean <- {

    Int32 value

	# a comment
    # one multiple
    # lines
    -- public
    -- final
	Method(Boolean x / Boolean b) &  <- {}
    -- public,final
	Method(Boolean x / Boolean b) && <- {}
	-- public,final -- Method(Boolean x / Boolean b) |  <- {}
	--public,final -- Method(Boolean x / Boolean b) || <- {}
	* public,final * Method(Boolean x / *given* Boolean b) || <- {}
	- public,final - Method(Boolean x /          ) ! <- {

        -- constraint : x : true

    }

	Method(Int32 x < Int32 b) sum,∑ <- {}

	Method(Boolean x < Int32 b) < <- {}

	Method(Boolean x < Int32 b) <= <- {}

	Method(Int32 x < Double64 b) + <- {}

	Method(Int32 x < Double64 b) √ <- {}

	Method(Int32 x < Double64 b) ∜ <- {}

	Method(Int32 x < Double64 b) ∫ <- {}

	Method(Int32 x < Double64 b) ⎇ <- {}

	Method(Int32 x < Double64 b) ⇲ <- {}

}

Class() Iterator <- {

	Method( / Boolean b) hashNext

	Method( / Object v) next


}

---------------------------------------------------


Prototype() p <-




===========================================
shortcuts
+ add(*,*)
/ div(*,*)





█
█ Adds two values
█
▒ scope:module
▒ shortcuted
main () <- () {

	Int32 x,y,z
    ** notnull(x)
	b <- sub(x,y)
	b <- add(x,y)

}



create (User user) <- () {
	user <- User
}

User {

	create (User user) <- () {
		user <- User:new()
	}


	|| Adds two values
	||
	** scope(public)
	add (UInt16 b) <- (Int32 x, Int32 y) {
		b <- x + (y * x)
		return
	}

}


--------------------------------------

include science.unlicense.library:api-system:Runtime.*

# Function documentation
#
# main entry point
#
-- public, main
Function( / Chars[] args) sayHello <- {
    println('Hello world')
}


========================================================
ANNOTATION
========================================================

 @ : manque de visibilité
 ! : agressif
 - : ambigue avec math substract
 -- : possible ambigue avec math decrement
 * : ambigue avec math multiply
 ** : ambigue avec math power
 :: : possible ambigue avec les paths/package  <-- SOLUTION
 <+> : trop long a taper
 <...> : ambigue avec operateur sub/over math  <-- SOLUTION

------ examples --------

 	-- public,final
	Method(Boolean x / Boolean b) && <- {}

 	@ public,final
	Method(Boolean x / Boolean b) && <- {}

 	! public,final
	Method(Boolean x / Boolean b) && <- {}

 	<public,final>
	Method(Boolean x / Boolean b) && <- {}

 	:: public,final
	Method(Boolean x / Boolean b) && <- {}

	-- public,final -- Method(Boolean x / Boolean b) |  <- {}

	--public,final-- Method(Boolean x / Boolean b) || <- {}

	* public,final * Method(Boolean x / *given* Boolean b) || <- {}

	** public,final ** Method(Boolean x / *given* Boolean b) || <- {}

	::public,final:: Method(Boolean x / ::given:: Boolean b) || <- {}

	@ public,final @ Method(Boolean x / *given* Boolean b) || <- {}

	Method(Boolean x /          ) work <- {

        -- constraint : x : true
		x <- 5
        :: constraint : x > 20

    }



========================================================
BRANCHEMENT
========================================================

------ examples --------


Function (/Int32 a) complicate <- {

    Int32 x

    case (a < 10)
        x <- 5
    case (a < 20)
        x <- 20
    case
        x <- 30
    case!


    loop (x < 10)
        x <- x + 1
    loop!

}


Function(/Int32 a) complicate <- {

    loop:bigname (x <- 1 | x < 10 | x <- x++)
        x <- x + 1
        loop:smallname
            case x > 10
                next:bigname
                stop:smallname
            case!
        loop!
    loop!

}


========================================================
EXCEPTION
========================================================

Error, Exception, Info, Log ?

Facts :
Unchecked exception are a problem for reliability.
Check exception can be painful to always handle.

Possible blocks with one-line writing :


loop (x <- 1 | x < 10 | x <- x++)
	work()
loop!

loop (x <- 1 | x < 10 | x <- x++) work()


case (x < 10)
    work()
case!

case (x < 10) work();

return
fail
safe
unsafe



------ examples --------

Function(Int32 a < Int32 b | Errors) bug <- {
    -> fail errors.push(Error.ERROR, "big bug")
}


Function(Int32 a < Int32 b | Errors) bug <- {
    case (x < 0) -> fail errors.push(Error.ERROR, "big bug")
}

Function(Int32 a < Int32 b | Errors) bug <- {
    errors.push(Error.ERROR, "big bug", new CustomReport(...))
	-> fail
}

Function(Int32 a < Int32 b | Errors) bug <- {
    errors.push(Error.WARNING, "small imprecision")
    -> return a * 10
}


Function(/) complicate <- {

    # full syntax to catch errors on multiple lines
    safe
        Int32 x <- bug()
        Int32 y <- bug2()
    safe
		errors.get(0)
    safe!

    # syntax ignoring errors
    safe
        Int32 x <- bug()
        Int32 y <- bug2()
    safe!

    # short syntax ignoring error for single instruction
    safe Int32 x <- bug()

    # short syntax pushing error up
    unsafe Int32 x <- bug()

}

========================================================
FUNCTIONAL
========================================================


------ examples --------

Method work <- {

	Function(Int32 a < Int32 b) fff <- {
		...
	}

	Int32 x <- fff(45)

	fff <- self.localMethod

}




========================================================
REFLECTION et IMPORT
========================================================


-- define : org.geotoolkit.map.Context
-- include org.maven.model.MavenProject
<+> include(org.geotoolkit.map.Context)
+ define : org.geotoolkit.map.Context
-- with {moduleGroup}:{moduleId}:{version?}:Math.Double
-- with {moduleGroup}:{moduleId}::Math.*



Module module  <- {moduleGroup}:{moduleId}:{version?}:
Package racine <- {moduleGroup}:{moduleId}:{version?}:.
Class double   <- {moduleGroup}:{moduleId}:{version?}:Math.Double
Variable pi    <- {moduleGroup}:{moduleId}:{version?}:Math.PI
Function sum   <- {moduleGroup}:{moduleId}:{version?}:Math.sum
Method add     <- {moduleGroup}:{moduleId}:{version?}:Math.Double.add
Resource icon  <- {moduleGroup}:{moduleId}:{version?}:/data/logo.png

la version est optionelle
Module module  <- {moduleGroup}:{moduleId}::
Package racine <- {moduleGroup}:{moduleId}::.
Class double   <- {moduleGroup}:{moduleId}::Math.Double
Variable pi    <- {moduleGroup}:{moduleId}::Math.PI
Function sum   <- {moduleGroup}:{moduleId}::Math.sum
Method add     <- {moduleGroup}:{moduleId}::Math.Double.add
Resource icon  <- {moduleGroup}:{moduleId}::/data/logo.png

------ examples --------

Parent Reflect
Resource <- science.unlicense.library:api-system::data/logo.png
Module p <- science.unlicense.library:api-system::
Package p <- science.unlicense.library:api-system::.
Package p <- science.unlicense.library:api-system::Runtime
Variable p <- science.unlicense.library:api-system::Math.PI
Class p <- science.unlicense.library:api-system::Math.Double
Function p <- science.unlicense.library:api-system::Math.sum
Method p <- science.unlicense.library:api-system::List.add
Method p <- science.unlicense.library:api-system:7.3:List.add

```
