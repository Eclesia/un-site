# Hello World

And as tradition expects, here is the **Hello World**.


```
use science.unlicense:core:System.println

main <- Function(Array<Chars> args) {
    println('Hello World')
}

```
