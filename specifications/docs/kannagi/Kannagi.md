
## Kannagi VM bytecode Draft

 
*Memory Map*
 
0x0000 0000 

0x0000 1XXX clavier/control 

0x0000 2XXX OpenGL 

0x0000 3XXX sound 

0x0000 4XXX UL64 

0x0010 0000 -0x0011 FFFF WRAM 

0x0012 0000 -0x0017 FFFF WRAM TAS 

0x0018 0000 -0x003F FFFF WRAM Pile 

0x0040 0000 -0x007F FFFF ROM 

 
Assembleur UL6420 (Unlisencised Lib , 64 bits , 2 octets opcode ,version 0 ) 
----------------------------------------------
 
8 registres de données D0 - D7 
8 registres d'adresses A0 - A7 (A7 = SP) 
8 registres de données flottant F0 - F7 
 
t : type de registre (0 : DATA , 1 : ADRESSE) 
r : registre (0 à 7) 
b : bits (00 : 8 bits , 01 : 16 bits , 10 : 32 bits , 11 : 64 bits ) 
 
---------------------------------------------------------------------- 
registre /registre : 00 bb trrr trrr  
immediate / registre 01 bb 0000 trrr 
immediate / adresse 01 bb 0001 0000  
immediate zero / registre 01 bb 0010 trrr  
 
registre /registre(adresse) : 10 bb -rrr +rrr 
registre(adresse) /registre : 11 bb -rrr +rrr  
 
mov 0001 
add 0010 
sub 0011 
mul 0100 
div 0101 
asl 0110 
asr 0111 
lsr 1000 
lsl 1001 
 
and 1010 
or  1011 
eor 1100 
 
cmp 1111 
 
00 00 NOP 
00 01 WAI 
 
00 10 JMP (4 octets) 
00 11 JML (8 octets) 
 
00 12 JSR (4 octets) 
00 13 RTS (4 octets) 
 
00 14 JSL (8 octets) 
00 15 RTL (8 octets) 
 
00 2X Bcc 
 
0001 MOVE 
 
0010 ADD 
0011 SUB 
 
0100 AND 
0101 OR 
0110 EOR 
0111 NAN 
 
1000 CMP 
 
1001 LSL 
1010 LSR 
1011 ASL 
1100 ASR 
 
1101 MUL 
1110 DIV 
 
---------------------------------------------------------------------- 
 
MOVE.B/W/L/LW 8-64 bits DATA/ADRESSE registre / DATA/ADRESSE registre: 
0001 00bb trrr trrr 
 
exemple
```
move.l  D0,D1 
move.b  A5,D2 
move.w  D6,A3 
move.lw A1,A4 
```
 
 
---------------------------------------------------------------------- 
 
MOVE.B/W/L/LW 8-64 bits DATA/ADRESSE(pointer) registre / DATA/ADRESSE(pointer) registre: 
0001 01bb trrr trrr 
 
exemple
```
move.b  D1,(A0) 
move.b  (A1),D3 
move.l  (A0),(A5) 
```
 
---------------------------------------------------------------------- 
 
CLR.B/W/L/LW 8-64 bits immediate zero / DATA/ADRESSE registre: 
0001 10bb 0000 trrr 
 
exemple
```
clr.b  D0 
clr.l  A5 
```
 
---------------------------------------------------------------------- 
 
MOVE.B/W/L/LW 8-64 bits immediate / DATA/ADRESSE registre: 
0001 10bb 0001 trrr 
 
exemple
```
move.b  #56,D0 
move.l  #45,A5 
```
 
---------------------------------------------------------------------- 
 
MOVE.B/W/L/LW 8-64 bits immediate / adresse: 
0001 10bb 0010 0000 
 
exemple
```
move.b  #56,$4568 
move.l  #45,$7951 
```
 
---------------------------------------------------------------------- 
 
MOVE.B/W/L/LW 8-64 bits DATA/ADRESSE registre / adresse: 
0001 10bb 0011 trrr 
 
exemple
```
move.b  D0,$4568 
move.l  A5,$7951 
```
 
---------------------------------------------------------------------- 
 
AND.B/W/L/LW 8-64 bits DATA/ADRESSE registre / DATA/ADRESSE registre: 
0010 00bb trrr trrr 
 
exemple
```
and.l  D0,D1 
and.b  A5,D2 
and.w  D6,A3 
and.lw A1,A4 
```
 
---------------------------------------------------------------------- 
 
AND.B/W/L/LW 8-64 bits immediate / DATA/ADRESSE  registre: 
0010 01bb 0000 trrr 

exemple
``` 
and.l  #$FF,D1 
and.l  #$FF,A2 
```
 
---------------------------------------------------------------------- 
 
AND.B/W/L/LW 8-64 bits immediate / DATA/ADRESSE  registre: 
0010 01bb 0001 trrr 

exemple
``` 
and.l  $FF86,D1 
and.l  $FF92,A2 
```
 
---------------------------------------------------------------------- 
 
OR.B/W/L/LW 8-64 bits DATA/ADRESSE registre / DATA/ADRESSE registre: 
0010 10bb trrr trrr 
 
exemple
```
or.l  D0,D1 
or.b  A5,D2 
or.w  D6,A3 
or.lw A1,A4 
```
 
---------------------------------------------------------------------- 
 
OR.B/W/L/LW 8-64 bits immediate / DATA/ADRESSE registre: 
0010 11bb 0000 trrr 

exemple
``` 
or.l  #$FF,D1 
or.l  #$FF,A2 
```
 
---------------------------------------------------------------------- 
 
OR.B/W/L/LW 8-64 bits adresse / DATA/ADRESSE registre: 
0010 11bb 0001 trrr 
 
 exemple
```
or.l  $FF75,D1 
or.l  $FF75,A2 
```
 
---------------------------------------------------------------------- 
 
EOR.B/W/L/LW 8-64 bits DATA/ADRESSE registre / DATA/ADRESSE registre: 
0100 00bb trrr trrr 
 
exemple
```
eor.l  D0,D1 
eor.b  A5,D2 
eor.w  D6,A3 
eor.lw A1,A4 
```
 
---------------------------------------------------------------------- 
 
EOR.B/W/L/LW 8-64 bits immediate / DATA/ADRESSE registre: 
0100 01bb 0000 trrr 

exemple
```
eor.l  #$FF,D1 
eor.l  #$FF,A2 
```

---------------------------------------------------------------------- 
 
EOR.B/W/L/LW 8-64 bits adresse / DATA/ADRESSE registre: 
0100 01bb 0001 trrr 
 
exemple
```
not.l  $FF75,D1 
not.l  $FF75,A2 
```
 
---------------------------------------------------------------------- 
 
NOT.B/W/L/LW 8-64 bits DATA/ADRESSE registre / DATA/ADRESSE registre: 
0100 10bb trrr trrr 

exemple
```
not.l  D0,D1 
not.b  A5,D2 
not.w  D6,A3 
not.lw A1,A4 
```
 
---------------------------------------------------------------------- 
 
NOT.B/W/L/LW 8-64 bits immediate / DATA/ADRESSE registre: 
0100 11bb 0000 trrr 

exemple
```
not.l  #$FF,D1 
not.l  #$FF,A2 
```
 
---------------------------------------------------------------------- 
 
NOT.B/W/L/LW 8-64 bits adresse / DATA/ADRESSE registre: 
0100 11bb 0001 trrr 
 
exemple
```
not.l  $FF75,D1 
not.l  $FF75,A2 
```



Examples
--------



MOVE.B/W/L/LW 8-64 bits DATA/ADRESSE registre / DATA/ADRESSE registre: 
0001 00bb trrr trrr 
 
exemple 
```
move.b  D0,D1 
```
 
On l'écrit donc en hexa 0x1001


---------------------------------------
 
           
```
 Registres +--0--+--1--+--2--+--3--+--4--+--5--+--6--+--7--+
           |     |     |     |     |     |     |     |     |
           +-----+-----+-----+-----+-----+-----+-----+-----+
          
move.b #5,D0 
  
 Registres +--0--+--1--+--2--+--3--+--4--+--5--+--6--+--7--+
           |  5  |     |     |     |     |     |     |     |
           +-----+-----+-----+-----+-----+-----+-----+-----+
 
move.b D0,D1
  
 Registres +--0--+--1--+--2--+--3--+--4--+--5--+--6--+--7--+
           |  5  |  5  |     |     |     |     |     |     |
           +-----+-----+-----+-----+-----+-----+-----+-----+
 
 add.b D0,D1
   
 Registres +--0--+--1--+--2--+--3--+--4--+--5--+--6--+--7--+
           |  5  |  10 |     |     |     |     |     |     |
           +-----+-----+-----+-----+-----+-----+-----+-----+
 
move.b d1,$7FFF
  
 Registres +--0--+--1--+--2--+--3--+--4--+--5--+--6--+--7--+
           |  5  |  10 |     |     |     |     |     |     |
           +-----+-----+-----+-----+-----+-----+-----+-----+
 Memoire 0x7FFF = 10
```
 