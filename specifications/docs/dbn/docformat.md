
Binary document format encoded as a file
----------------------------------------

This format defines the header and structure of files who contain
documents in binary form.


```text
Bytes 0               4               8               X               Y
      +---+---+---+---+---+---+---+---+---+ - - - +---+---+ - - - +---+
      |'D' 'B' 'N' ' '| 1             | DOCUMENT TYPE |    DOCUMENT   |
      +---+---+---+---+---+---+---+---+---+ - - - +---+---+ - - - +---+
```

|Offset| Description                                                                                             |
|-----|----------------------------------------------------------------------------------------------------------|
| 0-4 | file format signature, ASCII value "DBN " with the ending space                                          |
| 4-8 | 4 bytes unsigned integer in Little-Endian for the format version number, must be 1                       |
| 8-X | document type encoded as a document, see document binary format                                          |
| X-Y | document matching provided document type if provided, encoded as defined by the document binary format   |

