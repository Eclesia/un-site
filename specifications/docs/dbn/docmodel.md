
## 1. Model

DBN is build around a document model.
It could be classes, prototypes or structures depending on the programming language.

```text
Document {
    1 type       : DocumentType
    N fields     : Pair<Text,Value>
}

DocumentType {
    1 id         : Text
    N parents    : DocumentType
    N fields     : FieldType
    N attributes : Pair<Text,Value>
}

FieldType {
    1 id         : Text
    1 type       : ValueType
    1 minOcc     : Integer
    1 maxOcc     : Integer
    1 default    : Value
    N attributes : Pair<Text,Value>
}

ValueType {
    1 ptype      : primitive type enum
    N size       : Integer
    1 refType    : DocumentType
    1 inline     : Boolean
    1 encoding   : text encoding
}

```

## 2. Primitives types

Document field values are encoded in predefined types, the type is declared by the field
```ValueType.ptype``` which may take one of the following values.

Base types can be in Little-Endian or Big-Endian.

| CODE | NAME      | DESCRIPTION                                          | ENDIANNESS|
| -----|-----------|------------------------------------------------------|-----------|
| 0    | Bit bool  | 1 bit interpreted as a boolean value                 |           |
| 1    | Byte bool | 1 byte interpreted as a boolean value                |           |
| 2    | Int8      | signed byte                                          |           |
| 3    | UInt8     | unsigned byte                                        |           |
| 4    | Int16     | signed short (2 bytes)                               | LE        |
| 5    | UInt16    | unsigned short (2 bytes)                             | LE        |
| 6    | Int32     | signed integer (4 bytes)                             | LE        |
| 7    | UInt32    | unsigned integer (4 bytes)                           | LE        |
| 8    | Int64     | signed long (8 bytes)                                | LE        |
| 9    | UInt64    | unsigned long (8 bytes)                              | LE        |
| 10   | Float     | IEEE 754 (4 bytes)                                   | LE        |
| 11   | Double    | IEEE 754 (8 bytes)                                   | LE        |
| 12   | Int16     | signed short (2 bytes)                               | BE        |
| 13   | UInt16    | unsigned short (2 bytes)                             | BE        |
| 14   | Int32     | signed integer (4 bytes)                             | BE        |
| 15   | UInt32    | unsigned integer (4 bytes)                           | BE        |
| 16   | Int64     | signed long (8 bytes)                                | BE        |
| 17   | UInt64    | unsigned long (8 bytes)                              | BE        |
| 18   | Float     | IEEE 754 (4 bytes)                                   | BE        |
| 19   | Double    | IEEE 754 (8 bytes)                                   | BE        |
| 20   | LEB128    | variable size unsigned integer, (1 to 8 bytes)       |           |
| 21   | ULEB128   | variable size signed integer, (1 to 8 bytes)         |           |
| 22   | Text      | for strings                                          |           |
| 23   | Doc       | for sub documents                                    |           |
| 24   | Variable  | varying type, which can take any of the above values |           |
| 0x80 | Var Bits  | Mask used for 1 to 64 bits fields                    |           |


### 2.1 LEB128 and ULEB128

LEB128 and ULEB128 types are used to store signed and unsigned integer from on 1 to 8 bytes.
A description of this encoding is available on [Wikipedia](https://en.wikipedia.org/wiki/LEB128)

### 2.2 Text

Text is a very common structure encode as follow :
```text
Bytes 0       X             X+Size
      + - - - +---+ - - - +---+
      |  Size |   byte array  |
      + - - - +---+ - - - +---+
```
**Size** : size of the byte array following encoded as a ULEB128

**Byte array** : Text bytes encoded in character encoding defined by document field type


### 2.3 Variable

The varying type is unknown before reading the document.
All varying values are composed of a field description document and
followed by the value itself.
```text
Bytes 0               X               Y
      +---+ - - - +---+---+ - - - +---+
      |  Value type   |     Value     |
      +---+ - - - +---+---+ - - - +---+
```

**Value type** : Document describing a field value type

**Value** : field value encoding following previous type



## 3. Document type as a document

Once a document type has been defined in code it must be presented as a document to be encoded.


```text
+--------------------------------------------------------+
| Register type                                          |
+------------+--------------------+-------------+--------+
| FIELD NAME |  TYPE              | MIN,MAX OCC | INLINE |
+------------+--------------------+-------------+--------+
| docs       | Doc(Document type) | 0 - N       | false  |
+------------+--------------------+-------------+--------+
```
- **docs** : document list, first one is the primary one


```text
+---------------------------------------------------------+
| Document type                                           |
+------------+---------------------+-------------+--------+
| FIELD NAME |  TYPE               | MIN,MAX OCC | INLINE |
+------------+---------------------+-------------+--------+
| id         | Text                | 1 - 1       |        |
| parents    | Int32 LE            | 0 - N       |        |
| fields     | Doc(Field type)     | 0 - N       | false  |
| attributes | Doc(Attribute type) | 0 - N       | false  |
+------------+---------------------+-------------+--------+
```
- **id** : doctype ID
- **parents** : parent types index
- **fields** : doctype fields 
- **attributes** : doctype attributes



```text
+-----------------------------------------------------------+
| Field type                                                |
+------------+-----------------------+-------------+--------+
| FIELD NAME |  TYPE                 | MIN,MAX OCC | INLINE |
+------------+-----------------------+-------------+--------+
| id         | Text                  | 1 - 1       |        |
| minocc     | Int32 LE              | 1 - 1       |        |
| maxocc     | Int32 LE              | 1 - 1       |        |
| attributes | Doc(Attribute type)   | 0 - N       | false  |
| valuetype  | Doc(Field value type) | 1 - 1       | true   |
| default    | UInt8[]               | 0 - 1       |        |
+------------+-----------------------+-------------+--------+
```
- **id** : field ID
- **minocc** : field minimum occurrences
- **maxocc** : field maximum occurrences
- **attributes** : field attributes
- **valuetype** : field value type
- **default** : default value


```text
+--------------------------------------------------------+
| Field value type                                       |
+------------+--------------------+-------------+--------+
| FIELD NAME |  TYPE              | MIN,MAX OCC | INLINE |
+------------+--------------------+-------------+--------+
| ptype      | UInt8              | 1 - 1       |        |
| size       | VarUInt            | 0 - N       |        |
| reftype    | Int32 LE           | 0 - 1       |        |
| inline     | Boolean byte       | 0 - 1       |        |
| charenc    | Text               | 0 - 1       |        |
+------------+--------------------+-------------+--------+
```
- **ptype** : field type
- **size** : field array size
- **reftype** : field doc type, should be a reference index in the 'docs' field list
- **inline** : if document are stored inline
- **charenc** : field character encoding


```text
+-----------------------------------------------------------+
| Attribute type                                            |
+------------+-----------------------+-------------+--------+
| FIELD NAME |  TYPE                 | MIN,MAX OCC | INLINE |
+------------+-----------------------+-------------+--------+
| id         | Text                  | 1 - 1       |        |
| nbocc      | VarUInt               | 1 - 1       |        |
| valuetype  | Doc(Field value type) | 1 - 1       | true   |
| value      | UInt8[]               | 1 - 1       |        |
+------------+-----------------------+-------------+--------+
```
- **id** : attribute ID
- **nbocc** : attribute number of occurrences
- **valuetype** : attribute field value type
- **value** : attributes values encoded as defined by value type
