# Specifications

Public domain specifications. While ISO,W3C and many others write specification documents,
some areas remain uncovered or corrupted by patents.

Specifications available here have been made to fill those gaps.