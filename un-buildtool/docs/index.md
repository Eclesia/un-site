
Presentation
------------

Unlicense build tool is a simpliste but complete pipeline tool.
While it's main purpose is to build java projects it is only a driving tool which execute plugins in a defined order.
It is then possible to execute any kind of actions, deployement, tests, documentation, packaging, mailing, reporting,...
for any language.

Current version include plugins for :
- artifact deployment and download
- java code documentation creation
- java test execution
- JDK javac
- JDK jar
- JDK jlink
- Draft Jsweet
- Draft Android APK


Documentation result
--------------------
<img src="./Interface.png" alt="Main frame" style="width: 100%;"/>
