rm -R target
mkdir target
cp index.html target/
cp theme.css target/
cp -r img target/

#build markdown sites
cd specifications
mkdocs build
cd ..

cd un-editbit
mkdocs build
cd ..

cd un-edit3d
mkdocs build
cd ..

cd un-editwidget
mkdocs build
cd ..

cd un-explorer
mkdocs build
cd ..

cd un-database
mkdocs build
cd ..

cd un-server
mkdocs build
cd ..

cd un-buildtool
mkdocs build
cd ..

cd un-calculator
mkdocs build
cd ..

cd un-lib
mkdocs build
cd ..
