
Objective
---------

Replacing the main operating system tools such as thunar, nautilus, media-player, irfanview. An all in one file management tool.


User interface
--------------
<img src="./Interface.png" alt="Main frame" style="width: 100%;"/>

