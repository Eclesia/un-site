

<img src="path3745-0-0.png" width="160"/>

# What is this project ?

**This projects aim is to regroup all resources in public domain and organize them in a proper framework.**

While the current code is in java and builds for a jvm, its goal is also to get free from this licensed 
environment by providing its own compiler or vm. This goal isn't impossible, there are many developers and 
scientists who are altruists (or have been deceived by licenses). 

In fact we can find a bit of everything in 
different languages, the work is to regroup, unify and fill the remaining gaps.

**Can I use it in my project ? Yes !**

Everything here is in Public Domain, clone, copy, modify, do anything
you want, you don't even have to send back any modify you make, or cite us or whatever.
Still it would be fair that you don't cut out the original author tags and send us back bugfixes
or improvements if you do any.

# Native libraries

The project will support multiple systems without any side libraries. But in
some cases it is necessary to access hardware capabilities like printers, graphic cards or other devices.

Those dependencies are keep to a minimum with :

- **JVM** : [Jogamp](http://jogamp.org), [Usb4Java](http://usb4java.org)
- **Android** : [Jogamp](http://jogamp.org)

<img width="240" height="50" src="http://jogamp.org/images/jogamp_symbols/website_final_blue_jogl_346x70pel.png"/>

<img width="240" height="50" src="http://usb4java.org/images/logo.png"/>
