# Status 
<li>API</li>
    <ul>
    <li><span class="meter"> 90% </span>Character Encoding (Char,Chars,CharIterator,CharEncoding)</li>
    <li><span class="meter"> 90% </span>Collection (Collection,Sequence,Set,Stack,Dictionary,Iterator)</li>
    <li><span class="meter">100% </span>Event (Event,EventSource,EventListener)</li>
    <li><span class="meter"> 70% </span>Buffer/Cursor</li>
    <li><span class="meter"> 50% </span>Code</li>
    <li><span class="meter">100% </span>Predicate</li>
    <li><span class="meter"> 60% </span>Logging</li>
    <li><span class="meter"> 80% </span>Task</li>
    <li><span class="meter"> 30% </span>Time</li>
    <li><span class="meter"> 40% </span>Unit</li>
    <li><span class="meter">  5% </span>Language ISO-639</li>
    <li><span class="meter"> 60% </span>Translation</li>
    <li><span class="meter"> 80% </span>Tree/Node (Node,NodeType)</li>
    <li><span class="meter"> 80% </span>Graph (Graph,Vertex,Edge)</li>
    <li><span class="meter"> 80% </span>Document</li>
    <li><span class="meter"> 80% </span>Parameter</li>
    <li><span class="meter"> 50% </span>Concurrency</li>
    <li>Syntax</li>
    	<ul>
		<li><span class="meter"> 90% </span>Regex</li>
		<li><span class="meter"> 90% </span>Lexer</li>
		<li><span class="meter"> 90% </span>Parser</li>
		<li><span class="meter"> 90% </span>AST</li>
		<li><span class="meter"> 90% </span>Grammar</li>
    	</ul>
    <li>System </li>
        <ul>
        <li><span class="meter">  5% </span>Module manager</li>
        <li><span class="meter"> 60% </span>Tree registry</li>
        <li><span class="meter"> 40% </span>Socket</li>
        </ul>
    <li>Device </li>
        <ul>
        <li><span class="meter">20% </span>USB</li>
        </ul>
    </ul>
<li>Science</li>
    <ul>
    <li>Mathematic</li>
        <ul>
        <li><span class="meter"> 90% </span>Vector-Tuple</li>
        <li><span class="meter"> 80% </span>Matrix</li>
        <li><span class="meter"> 70% </span>Quaternion</li>
        <li><span class="meter"> 60% </span>Linear algebra : LU, QR, Cholesky, Eigen value, Singular value</li>
        <li><span class="meter">100% </span>Interval</li>
        <li><span class="meter">100% </span>Complex number</li>
        <li><span class="meter"> 40% </span>Large integer</li>
        <li><span class="meter"> 70% </span>Large decimal</li>
        <li><span class="meter"> 40% </span>Interpolator</li>
        <li><span class="meter"> 70% </span>Diff engine</li>
        <li>Transforms</li>
            <ul>
            <li><span class="meter">60% </span>DCT/IDCT - Discrete Cosinus Transform</li>
            <li><span class="meter">10% </span>MDCT/IMDCT - Modified Discrete Cosinus Transform</li>
            <li><span class="meter">60% </span>DFT/IDFT - Discrete Fourrier Transform</li>
            <li><span class="meter">60% </span>FFT/IFFT - Fast Fourrier Transform</li>
            <li><span class="meter">60% </span>ZigZag</li>
            <li><span class="meter">60% </span>Quantification</li>
            </ul>
        </ul>
        <li>Algorithms</li>
        	<ul>
		    <li><span class="meter"> 90% </span>Median cut</li>
		    <li><span class="meter"> 90% </span>K-Means</li>
        	</ul>
    <li>Image</li>
        <ul>
        <li><span class="meter"> 80% </span>Image (and iterator, meta model, N dimensions)</li>
        <li><span class="meter"> 80% </span>ImageIO (reader, writer, parameters)</li>
        <li><span class="meter"> 80% </span>Colorspace, Color model, Sample model</li>
        <li><span class="meter">100% </span>Porter-Duff rules</li>
        <li>Operators</li>
            <ul>
            <li><span class="meter">60% </span>Convolution (Sobel, Prewitt, LaPlace, Mdif, Smooth, Blur, Emboss, Gaussian)</li>
            <li><span class="meter">60% </span>Detection (Canny, MarchinSquares, Symmetry)</li>
            <li><span class="meter">60% </span>Noise (Gaussian, Uniform, Simplex)</li>
            <li><span class="meter">60% </span>Morphologic (Erode, Dilate, Open, Close)</li>
            <li><span class="meter">90% </span>Geometric (Vertical/Horizontal flip, Rotate 90/180/270, Rescale, Transform)</li>
            <li><span class="meter">70% </span>Paint (Recolorize,Rotate)</li>
            <li><span class="meter">60% </span>Analysis (Difference,Threshold)</li>
            <li><span class="meter">60% </span>Haralick</li>
            <li><span class="meter">60% </span>Watershed by grayscale levels</li>
            <li><span class="meter">80% </span>Chamfer distance map</li>
            <li><span class="meter">80% </span>Signed distance field</li>
            <li><span class="meter">60% </span>SeamCarving</li>
            </ul>
        </ul>        
    <li>Media</li>
        <ul>
        <li><span class="meter">60% </span>MediaStore (stream meta, reader, format)</li>
        <li><span class="meter">60% </span>Audio</li>
        <li><span class="meter">60% </span>Video</li>
        <li><span class="meter"> 5% </span>subtitle</li>
        </ul>      
    <li>Geometry</li>
        <ul>
        <li><span class="meter"> 80% </span>2D (Point,Line,Circle,Ellipse,Polyline,Polygone,Path, ...)</li>
        <li><span class="meter"> 60% </span>3D (Sphere,Ellipsoid,Capsule,Ray,Plan,Cone,Cylinder ...)</li>
        <li><span class="meter"> 80% </span>ND (BoundingBox)</li>
        <li>Analyze(distance,intersection,collision,contains,...)(in progress)</li>
            <ul>
            <li>2D <a class="link" href="./geometry2D.html" target="blank">(see status tables)</a></li>
            <li>3D <a class="link" href="./geometry3D.html" target="blank">(see status tables)</a></li>
            </ul>
        <li><span class="meter"> 80% </span>Delaunay-Voronoi triangulation</li>
        <li><span class="meter"> 80% </span>Ear-Clipping triangulation</li>
        <li>Index</li>
        	<ul>
		    <li><span class="meter"> 70% </span>QuadTree</li>
		    <li><span class="meter"> 70% </span>OctTree</li>
        	</ul>
        </ul>
    <li>Geospatial</li>
        <ul>
		    <li>Transforms</li>
		    <ul>
		    <li><span class="meter"> 20% </span>Orthographic</li>
		    <li><span class="meter"> 20% </span>Gnomonique</li>
		    <li><span class="meter"> 20% </span>Stereographic</li>
		    <li><span class="meter"> 20% </span>Mercator</li>
        	</ul>
        </ul>
    <li>Layouts</li>
        <ul>
        <li><span class="meter"> 90% </span>Absolute layout</li>
        <li><span class="meter"> 90% </span>Grid layout</li>
        <li><span class="meter"> 90% </span>Border layout</li>
        <li><span class="meter"> 90% </span>Form layout</li>
        <li><span class="meter"> 80% </span>Circular layout</li>
        <li><span class="meter"> 90% </span>Pair layout</li>
        <li><span class="meter"> 90% </span>Block layout</li>
        <li><span class="meter"> 90% </span>Line layout</li>
        <li><span class="meter"> 80% </span>Displacement layout</li>
        </ul>
    <li>Physic</li>
        <ul>
        <li><span class="meter"> 40% </span>Collision world</li>
        <li><span class="meter"> 40% </span>Particle, RigidBody</li>
        <li><span class="meter"> 20% </span>Force,Singularity,Constraint</li>
        <li><span class="meter">  5% </span>Torque</li>
        <li><span class="meter"> 60% </span>Skeleton</li>
        <li><span class="meter"> 60% </span>Inverse Kinematic : CCD</li>
        <li><span class="meter">  0% </span>Inverse Kinematic : Jacobian</li>
        <li><span class="meter"> 40% </span>Integrator : Euler</li>
        <li><span class="meter"> 20% </span>Integrator : Verlet</li>
        <li><span class="meter"> 80% </span>Ohm-Law</li>
        </ul>
    <li>Animation</li>
        <ul>
        <li><span class="meter"> 80% </span>TimeSerie</li>
        <li><span class="meter"> 80% </span>KeyFrame</li>
        <li><span class="meter"> 60% </span>Interpolator</li>
        <li><span class="meter"> 80% </span>Property sync</li>
        <li><span class="meter"> 40% </span>Timer</li>
        </ul>
    </ul>
<li>Encoding</li>    
    <ul>
    <li><span class="meter"> 90% </span>BigEndian/LittleEndian/LEB128</li>
    <li><span class="meter"> 90% </span>In/Out Byte Stream</li>
    <li><span class="meter"> 90% </span>In/Out Data Stream (bits,byte,short,ushort,int,uint,vint,long,float,double)</li>
    <li><span class="meter"> 80% </span>In/Out Char Stream</li>
    <li>Checksum/HashFunction/Crypto</li>
        <ul>
        <li><span class="meter"> 100% </span>AES 128,192,256</li>
        <li><span class="meter">  80% </span>CRC16,32,64</li>
        <li><span class="meter">  60% </span>Adler32</li>
        <li><span class="meter"> 100% </span>SHA-1,224,256,384,512</li>
        <li><span class="meter"> 100% </span>MD2,4,5</li>
        <li><span class="meter">  60% </span>PBKDF2</li>
        <li><span class="meter"> 100% </span>PKCS5</li>
        <li><span class="meter"> 100% </span>CBC</li>
        <li><span class="meter"> 100% </span>CTR</li>
        <li><span class="meter">  70% </span>RSA</li>
        <li><span class="meter">  60% </span>RC4</li>
        <li><span class="meter">  10% </span>Salsa20</li>
        <li><span class="meter">  90% </span>Skein-512</li>
        <li><span class="meter">  70% </span>Elliptic Curve</li>
        <li><span class="meter">  60% </span>In/Out hashfunction byte stream</li>
        </ul>
    <li><span class="meter"> 90% </span>Color (RGBA, YUV, YCbCr, HSV, HSL, CIE, Grayscale, Hexa,...)</li>
    <li><span class="meter"> 80% </span>Base64 (read,write)</li>
    <li><span class="meter"> 80% </span>Base85/Ascii85/Z85 (read,write)</li>
    <li><span class="meter"> 30% </span>ECMA-48 (X3.64 - ISO/IEC 6429 : console/terminal sequences)</li>
    <li><span class="meter"> 40% </span>URLEncoder</li>
    <li>Compression</li>
        <ul>
        <li><span class="meter"> 80% </span><span class="meter"> 40% </span>LZMA</li>
        <li><span class="meter"> 80% </span><span class="meter"> 80% </span>LZSS</li>
        <li><span class="meter"> 80% </span><span class="meter"> 40% </span>Deflate</li>
        <li><span class="meter"> 80% </span><span class="meter"> 40% </span>Zlib</li>
        <li><span class="meter"> 80% </span><span class="meter"> 40% </span>GZip</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>RLE</li>
        </ul>
    </ul>
<li>Protocol</li>    
    <ul>
    <li><span class="meter"> 100% </span>Echo</li>
    <li><span class="meter">  20% </span>HTTP</li>
    <li><span class="meter">  20% </span>IRC</li>
    <li><span class="meter">   5% </span>POP</li>
    <li><span class="meter">   5% </span>IMAP</li>
    <li><span class="meter">   5% </span>SMTP</li>
    <li><span class="meter">   5% </span>Tetnet</li>
    </ul>
<li>Storage</li>
    <ul>
    <li>Binding</li>
        <ul>
        <li><span class="meter"> 90% </span><span class="meter"> 90% </span>In/Out XML Stream</li>
			<ul>
			<li><span class="meter"> 80% </span><span class="meter"> 10% </span>XSD</li>
			<li><span class="meter"> 10% </span>XPath</li>
			</ul>
        <li><span class="meter"> 60% </span>DOM API</li>
        <li><span class="meter"> 90% </span><span class="meter"> 60% </span>DOM</li>
        <li><span class="meter"> 70% </span><span class="meter"> 70% </span>DBN</li>
        <li><span class="meter"> 90% </span><span class="meter"> 80% </span>JSON</li>
        <li><span class="meter"> 90% </span><span class="meter">  0% </span>HJSON</li>
        <li><span class="meter"> 80% </span><span class="meter">  0% </span>XSD</li>
        <li><span class="meter"> 80% </span><span class="meter"> 20% </span>RIFF</li>
        <li><span class="meter"> 80% </span><span class="meter">  0% </span>OLE</li>
        <li><span class="meter"> 80% </span><span class="meter">  0% </span>EBML</li>
        <li><span class="meter"> 90% </span><span class="meter">  0% </span>M3U</li>
        <li><span class="meter"> 10% </span>Automatic class generation from XSD</li>
        </ul>
    <li>FileSystem</li>
        <ul>
        <li><span class="meter">  0% </span><span class="meter">  0% </span>EXT-3</li>
        <li><span class="meter">  0% </span><span class="meter">  0% </span>EXT-4</li>
        <li><span class="meter">  0% </span><span class="meter">  0% </span>FAT</li>
        <li><span class="meter"> 30% </span><span class="meter">  0% </span>NTFS</li>
        <li><span class="meter">  0% </span><span class="meter">  0% </span>ZFS</li>
        <li><span class="meter"> 10% </span><span class="meter"> 10% </span>Fuse</li>
        </ul>
    <li>Archive</li>
        <ul>
        <li><span class="meter">  5% </span><span class="meter"> 0% </span>7Z</li>
        <li><span class="meter">  0% </span><span class="meter"> 0% </span>RAR</li>
        <li><span class="meter"> 20% </span><span class="meter"> 0% </span>TAR</li>
        <li><span class="meter"> 70% </span><span class="meter"> 70% </span>XZ</li>
        <li><span class="meter"> 80% </span><span class="meter"> 10% </span>ZIP</li>
        </ul>
    <li>Document</li>
        <ul>
        <li><span class="meter">  0% </span><span class="meter"> 0% </span>MarkDown</li>
        <li><span class="meter">  0% </span><span class="meter"> 0% </span>OfficeOpenXML</li>
        <li><span class="meter">  0% </span><span class="meter"> 0% </span>OpenDocument</li>
        <li><span class="meter">  0% </span><span class="meter"> 0% </span>RTF</li>
        <li><span class="meter">  0% </span><span class="meter"> 0% </span>YAML</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>CSV</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>INI</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>GeoJson</li>
        </ul>
    <li>Image</li>
        <ul>
        <li><span class="meter">90% </span><span class="meter"> 0% </span>Ascii-Grid</li>
        <li><span class="meter">10% </span><span class="meter"> 0% </span>ANI</li>
        <li><span class="meter">80% </span><span class="meter">20% </span>BMP</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>CUR</li>
        <li><span class="meter">70% </span><span class="meter"> 0% </span>DDS (DXT 1-5,ATI 1-2, DX10)</li>
        <li><span class="meter">90% </span><span class="meter">10% </span>GIF</li>
        <li><span class="meter"> 5% </span><span class="meter"> 0% </span>EXR</li>
        <li><span class="meter"> 5% </span><span class="meter"> 0% </span>GRIB 1/2</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>ICO</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>KTX</li>
        <li><span class="meter">70% </span><span class="meter"> 0% </span>JPEG</li>
        <li><span class="meter"> 5% </span><span class="meter"> 0% </span>JPEG-2000</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>NetCDF</li>
        <li><span class="meter">80% </span><span class="meter">80% </span>PBM/PGM/PPM ASCII/Binary</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>PCX</li>
        <li><span class="meter">70% </span><span class="meter">50% </span>PNG</li>
        <li><span class="meter"> 5% </span><span class="meter"> 0% </span>RAS</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>RAW</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>SGI</li>
        <li><span class="meter">90% </span><span class="meter"> 0% </span>TGA</li>
        <li><span class="meter">60% </span><span class="meter"> 0% </span>TIFF</li>
        <li><span class="meter">40% </span><span class="meter"> 0% </span>WBMP</li>
        <li><span class="meter">60% </span><span class="meter">40% </span>XBM</li>
        <li><span class="meter">40% </span><span class="meter"> 0% </span>XPM</li>
        </ul>
    <li>Font</li>
        <ul>
        <li><span class="meter"> 10% </span><span class="meter"> 0% </span>OpenTypeFont</li>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>TrueTypeFont</li>
        </ul>
    <li>Media</li>
        <ul>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>AAC</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>APNG</li>
        <li><span class="meter"> 20% </span><span class="meter"> 5% </span>AVI</li>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>Flac</li>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>FLV</li>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>F4V</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>GIF-Animated</li>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>IVF</li>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>MKV</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>Mpeg-1</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>Mpeg-2</li>
        <li><span class="meter"> 50% </span><span class="meter"> 0% </span>Mpeg-4</li>
        <li><span class="meter">  5% </span><span class="meter"> 0% </span>MP3</li>
        <li><span class="meter">  5% </span><span class="meter"> 0% </span>OGG Stream</li>
        <li><span class="meter">  5% </span><span class="meter"> 0% </span>OGG Vorbis</li>
        <li><span class="meter"> 60% </span><span class="meter"> 20% </span>SWF</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>VP8</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>VP8-L</li>
        <li><span class="meter"> 80% </span><span class="meter"> 40% </span>WAV</li>
        </ul>
    <li>Model 2d</li>
        <ul>
        <li><span class="meter"> 50% </span><span class="meter">  0% </span>PostScript+VM</li>
        <li><span class="meter"> 60% </span><span class="meter">  0% </span>SVG</li>
        <li><span class="meter"> 90% </span><span class="meter"> 90% </span>WKB</li>
        <li><span class="meter"> 90% </span><span class="meter"> 90% </span>WKT</li>
        <li><span class="meter"> 80% </span><span class="meter"> 80% </span>GCode</li>
        </ul>
    <li>Model 3d</li>
        <ul>
        <li><span class="meter"> 20% </span><span class="meter"> 0% </span>3DS</li>
        <li><span class="meter"> 70% </span><span class="meter"> 0% </span>Blender</li>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>BVH motion</li>
        <li><span class="meter">  5% </span><span class="meter"> 0% </span>Collada</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>FBX</li>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>LWO</li>
        <li><span class="meter"> 10% </span><span class="meter"> 0% </span>Maya</li>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>MQO</li>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>MTL</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>MS3D</li>
        <li><span class="meter"> 60% </span><span class="meter"> 0% </span>OBJ</li>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>OFF</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>PLY</li>
        <li><span class="meter"> 80% </span><span class="meter"> 0% </span>STL ASCII/Binary</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>TDCG</li>
        <li><span class="meter"> 30% </span><span class="meter"> 0% </span>X</li>
        <li>Quake/Doom (Id Software)</li>
        	<ul>
		    <li><span class="meter"> 80% </span><span class="meter"> 0% </span>MD2</li>
		    <li><span class="meter"> 80% </span><span class="meter"> 0% </span>MD3</li>
		    <li><span class="meter"> 80% </span><span class="meter"> 0% </span>MD5</li>
        	</ul>
        <li>Source (Valve)</li>
        	<ul>
        	<li><span class="meter"> 60% </span><span class="meter"> 0% </span>MDL</li>
        	<li><span class="meter"> 60% </span><span class="meter"> 0% </span>VTF</li>
        	<li><span class="meter"> 60% </span><span class="meter"> 0% </span>VTX</li>
        	<li><span class="meter"> 60% </span><span class="meter"> 0% </span>VVD</li>
        	</ul>
        <li>MMD (MikuMikuDance)</li>
            <ul>
            <li><span class="meter"> 80% </span><span class="meter"> 60% </span>PMD</li>
            <li><span class="meter"> 80% </span><span class="meter"> 40% </span>PMX</li>
            <li><span class="meter"> 80% </span><span class="meter"> 40% </span>VMD</li>
            <li><span class="meter"> 80% </span><span class="meter"> 40% </span>VPD</li>
            </ul>
        <li>Unity</li>
        	<ul>
		    <li><span class="meter"> 70% </span><span class="meter"> 10% </span>UNITY3D</li>
		    <li><span class="meter"> 40% </span><span class="meter"> 10% </span>ASSETS</li>
		    <li><span class="meter">  0% </span><span class="meter"> 0% </span>UNITYPACKAGE</li>
        	</ul>
        <li>XNA (XnaLara)</li>
            <ul>
            <li><span class="meter"> 80% </span><span class="meter"> 60% </span>POSE</li>
            <li><span class="meter"> 80% </span><span class="meter">  0% </span>MESH.ASCII</li>
            <li><span class="meter"> 80% </span><span class="meter">  0% </span>XPS/MESH</li>
            <li><span class="meter">  5% </span><span class="meter">  0% </span>XNB</li>
            </ul>
        </ul>
    <li>Other</li>
        <ul>
        <li><span class="meter"> 40% </span><span class="meter"> 0% </span>CSS</li>
        </ul>
    </ul>
<li>Engine</li>
    <ul>
    <li>On CPU :</li>
        <ul>
        <li><span class="meter"> 60% </span>Rasterizer2D (ImagePainter2D)</li>
        </ul>
    <li>AWT/Swing :</li>
        <ul>
        <li><span class="meter"> 50% </span>Painter2D on Graphics2D</li>
        </ul>
    <li>Opengl : a 3D scenograph</li>
        <ul>
        <li><span class="meter"> 80% </span>Rasterizer2D (ImagePainter2D)</li>
        <li><span class="meter"> 80% </span>Left or Right Handed coordinate system</li>
        <li><span class="meter"> 80% </span>Node</li>
        <li><span class="meter"> 80% </span>Mesh</li>
        <li><span class="meter"> 60% </span>Camera</li>
        <li><span class="meter"> 60% </span>Light</li>
        <li><span class="meter"> 60% </span>Tesselation</li>
        <li><span class="meter"> 50% </span>TransformFeedBack</li>
        <li><span class="meter"> 80% </span>Instancing</li>
        <li><span class="meter"> 60% </span>NavigationMap</li>
        <li><span class="meter"> 80% </span>Shaders : Program,Template,Uniform</li>
        <li><span class="meter"> 70% </span>Resource : Texture, FBO, VBO, VAO, IBO, PBO, RBO, UBO, TBO, Shader,...</li>
        <li><span class="meter"> 80% </span>Mapping : UV,Cube,Spherical,DualParaboloid,Cylinder,DistanceField</li>
        <li><span class="meter"> 80% </span>Sprite</li>
        <li><span class="meter"> 80% </span>Offscreen and Transparent frames</li>
        <li><span class="meter"> 80% </span>Deferred Shading</li>
        <li><span class="meter"> 10% </span>Procedural terrain</li>
        <li><span class="meter"> 60% </span>Particle system</li>
        <li>Animation</li>
            <ul>
            <li><span class="meter"> 80% </span>Skinning : by weight</li>
            <li><span class="meter"> 70% </span>Skinning : spherical</li>
            <li><span class="meter">  0% </span>Skinning : dual quaternion</li>
            <li><span class="meter"> 80% </span>Morph target</li>
            </ul>
        <li>Effects</li>
            <ul>
            <li><span class="meter"> 40% </span>Fog</li>
            <li><span class="meter"> 40% </span>Bloom/Glow</li>
            <li><span class="meter"> 80% </span>Convolution</li>
            <li><span class="meter"> 80% </span>Fast gaussian blur</li>
            <li><span class="meter"> 80% </span>Depth of field</li>
            <li><span class="meter"> 40% </span>Silhouette</li>
            <li><span class="meter"> 60% </span>Cell/Toon shading</li>
            <li><span class="meter"> 60% </span>FXAA</li>
            <li><span class="meter"> 60% </span>SSAO</li>
            <li><span class="meter"> 10% </span>Blend</li>
            <li><span class="meter"> 40% </span>Projected Shadow</li>
            <li><span class="meter"> 80% </span>Reflection textures</li>
            </ul>
        <li>Debug tools</li>
            <ul>
            <li><span class="meter"> 80% </span>WireFrame view</li>
            <li><span class="meter"> 40% </span>Skeleton view</li>
            <li><span class="meter"> 80% </span>Normals view</li>
            <li><span class="meter"> 80% </span>Face normals view</li>
            <li><span class="meter"> 80% </span>Triangle adjency view</li>
            <li><span class="meter"> 60% </span>Camera frustrum view</li>
            <li><span class="meter"> 80% </span>BBox view</li>
            </ul>
        <li>Controller</li>
            <ul>
            <li><span class="meter"> 60% </span>Orbit</li>
            <li><span class="meter"> 60% </span>FPS</li>
            <li><span class="meter"> 60% </span>Plane</li>
            </ul>
        <li><span class="meter"> 15% </span>Speaker</li>
        </ul>
    </ul>
<li>User Interface</li>
    <ul>
    <li><span class="meter"> 80% </span>Scene API</li>
    <li><span class="meter"> 80% </span>RuleStyleSheet</li>
    <li><span class="meter"> 80% </span>Property binding</li>
    <li><span class="meter"> 80% </span>Cursor</li>
    <li><span class="meter"> 40% </span>Frame decoration</li>
    <li><span class="meter"> 60% </span>Drag&Drop</li>
    <li><span class="meter"> 40% </span>Clipboard</li>
    <li>Widgets</li>
        <ul>
        <li>Menu</li>
            <ul>
            <li><span class="meter"> 80% </span>WButtonBar</li>
            <li><span class="meter"> 80% </span>WMenuButton</li>
            <li><span class="meter"> 60% </span>WMenuDropDown</li>
            </ul>
        <li>Desktop</li>
            <ul>
            <li><span class="meter"> 80% </span>WDesktop</li>
            <li><span class="meter"> 80% </span>WDesktopFrame</li>
            <li><span class="meter">  0% </span>WDesktopBar</li>
            </ul>
        <li><span class="meter"> 80% </span>WLabel</li>
        <li><span class="meter"> 80% </span>WButton</li>
        <li><span class="meter"> 60% </span>WSwitch</li>
        <li><span class="meter"> 60% </span>WSelect</li>
        <li><span class="meter"> 80% </span>WCheck</li>
        <li><span class="meter"> 70% </span>WTextField</li>
        <li><span class="meter"> 70% </span>WTextArea</li>
        <li><span class="meter"> 60% </span>WProgressBar</li>
        <li><span class="meter"> 70% </span>WSlider</li>
        <li><span class="meter"> 70% </span>WRangeSlider</li>
        <li><span class="meter"> 80% </span>WSpinner</li>
        <li><span class="meter"> 80% </span>WSeparator</li>
        <li><span class="meter"> 80% </span>WList</li>
        <li><span class="meter"> 80% </span>WTree</li>
        <li><span class="meter"> 80% </span>WTreeTable</li>
        <li><span class="meter"> 80% </span>WTabContainer</li>
        <li><span class="meter"> 60% </span>WPopup</li>
        <li><span class="meter"> 80% </span>WTable</li>
        <li><span class="meter"> 60% </span>WCrumbBar</li>
        <li><span class="meter"> 60% </span>WGLCanvas</li>
        </ul>
    <li>Components</li>
        <ul>
        <li><span class="meter"> 50% </span>WCrumbBar</li>
        <li><span class="meter"> 80% </span>WPathChooser</li>
        <li><span class="meter"> 50% </span>WColorChooser</li>
        <li><span class="meter">  0% </span>WDateChooser</li>
        </ul>
    </ul>
<li>Compiler and Language</li>
    <ul>
    <li><span class="meter">  10% </span>BNF</li>
    <li><span class="meter">   5% </span>BootBasicVM</li>
    <li><span class="meter">  10% </span><span class="meter">  0% </span>Portable Executable</li>
    <li><span class="meter">  20% </span><span class="meter">  0% </span>ELF</li>
    <li>Syntax</li>
		<ul>
		<li><span class="meter"> 50% </span><span class="meter">  0% </span>ActionScript3</li>
    	<li><span class="meter">  5% </span><span class="meter">  0% </span>BootBasic</li>
		<li><span class="meter"> 40% </span><span class="meter">  0% </span>C-Preprocessor</li>
		<li><span class="meter"> 20% </span><span class="meter">  0% </span>C</li>
		<li><span class="meter"> 20% </span><span class="meter">  0% </span>GLSL</li>
		<li><span class="meter"> 70% </span><span class="meter"> 20% </span>Java</li>
		<li><span class="meter"> 80% </span><span class="meter">  0% </span>WebAssembly</li>
		</ul>
	<li>ByteCode</li>
		<ul>
		<li><span class="meter"> 50% </span><span class="meter">  0% </span>ActionScript3</li>
		<li><span class="meter"> 80% </span><span class="meter"> 80% </span>SPIR-V (Vulcan)</li>
		</ul>
    </ul>
