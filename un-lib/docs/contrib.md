
# Developers

- Johann Sorel
- Remi Bonnaud
- Aur&eacute;lien Lombardo
- Bertrand Cote
- Francois Berder [Web site](https://developer.mbed.org/users/feb11/code/Crypto/)
- Samuel Andrés

# Direct contributors

- Yann D'Isanto
- Xavier Philippeau
- Thibaut Cuvelier
- Florent Humbert [Millie project](http://projets.developpez.com/projects/millie/)
- Xerces Ranby
- Izyumov Konstantin [Github](https://github.com/Konstantin8105/Triangulation)

# Undirect contributors (by origin)

**Castor3D** [Web site](http://projets.developpez.com/projects/castor3d)

- Sylvain Doremus [Web site](http://www.dragonjoker.fr)

**Djehuty** [Web site](https://github.com/wilkie/djehuty)

- Dave Wilkinson
- Lindsey Bieda

**GL/Shader tutorials** [Web site](http://antongerdelan.net/opengl/)

- Anton Gerdelan 

**IHarder/base64 compression** [Web site](http://iharder.sourceforge.net)

- Robert Harder 

**JAMA linear algebra** [Web site](http://math.nist.gov/javanumerics/jama/)

- Joe Hicklin
- Cleve Moler
- Peter Webb
- Ronald F. Boisvert
- Bruce Miller
- Roldan Pozo
- Karin Remington

**Java Advanced Audio Decoder** [Web site](http://jaadec.sourceforge.net)

- Alexander Simm

**JSpatial** [Web site](http://mvn.io7m.com/io7m-jspatial/)

- Mark Raynsford

**Tukaani, XZ Compression** [Web site](http://tukaani.org/xz/java.html)

- Lasse Collin
- Igor Pavlov
- Stefan Bodewig
- Carl Hasselskog
- Arunesh Mathur
- Jim Meyering
- Benoit Nadeau
- Christian Schlichtherle
- Alyosha Vasilieva

**Javatar compression**

- Timothy Gerard Endres 
- David. M. Gaskin

**LZMA compression**

- Igor Pavlov

**Nothings.org** [Web site](http://nothings.org/)

- Sean Barrett (very usefull resource on jpeg reader and rasterizer) 

**Tuffy font** [Web site](http://tulrich.com)

- Thatcher Ulrich

**Tomgibara.com** [Web site](http://www.tomgibara.com/computer-vision/)

- Tom Gibara (Canny and Symmetry detection, marching squares, Haar wavelets)

**Traer Physics 3.0** [Web site](http://murderandcreate.com/physics/)

- Jeffrey Traer Bernstein

**SHA-1**

- Steve Reid
- Chuck McManis
- M.Kiesel

**MD5**

- jonh Exp
- Jon Howell
- Tatu Ylonen

**Elliptic Curve Cryptography** [Web site](http://bmsi.com/java/#EC)

- George Barwood
- Paulo S.L.M. Barreto
- Stuart D. Gathman

**Diff engine** [Web site](http://www.codeproject.com/Articles/6943/A-Generic-Reusable-Diff-Algorithm-in-C-II) 

- Michael Potter

**Image trace algorithm** [Web site](https://github.com/jankovicsandras/imagetracerjava)

- jankovics andras

**Literateprograms, various algorithms** [Web site](https://en.literateprograms.org)

- Spoon! (I couldn't find the real name)

**PadSynth** [Web site](http://zynaddsubfx.sourceforge.net/doc/PADsynth/PADsynth.htm)

- Nasca Octavian Paul

**Distance field shader** [Web site](http://contourtextures.wikidot.com)

- Stefan Gustavson

**LZSS** [Web site](http://home.worldonline.cz/~cz210552/lzss.html) [Cache](http://en.pudn.com/downloads121/sourcecode/java/detail516351_en.html)

- Haruhiko Okumura
- Shawn Hargreaves

**LargeDecimal** [Web site](http://dev.dwbrite.com/post/143271862389/this-blog-post-is-mostly-just-a-test-for-code)

- Devin W. Brite	

**Concurrency package** [Web site](http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/intro.html) 

- Doug Lea	


-------------------------------------------
If you are part of this list (or missing) and would like to have more informations displayed or removed, contact johann sorel on Bitbucket.


