
Objective
---------

Create the first public domain NoSQL Document Database.

The database communicates using the DBN (Document as Binary) format both for requests and responses.
This approaches allows a verify flexible and extendable model .


### Store ###
- in memory : for simple use
- on single archive : for convient small volumes, personnal needs or exchange
- on file folder : for performance storage and big volumes
- distante : for network and multiple clients

### Models ###
- Table : well known design model for all purpose used with SQL queries
- Document : convinient model to store and search in documents
- Link : unlike foreign key in tables, links are direct references to objets, transversal query are more efficient, this approach is mostly used in graph models.

### Plugins ###
Existing databases have very week models which make extensions very difficult or impossible, this is a crucial point for modern databases. they must be multi-purpose.
Plugins will be able to add :

- new field types
- new index types
- new functions
- new connexion and query protocols

